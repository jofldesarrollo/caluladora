package com.prueba.caluladora;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements CalculadoraFragment.OnFragmentInteractionListener,GraficoFragment.OnFragmentInteractionListener,TerminosFragment.OnFragmentInteractionListener,AyudaFragment.OnFragmentInteractionListener {

    private TextView mTextMessage;
    //This is our viewPager
    private ViewPager viewPager;


    //Fragments

    AyudaFragment ayudaFragment;
    CalculadoraFragment calculadoraFragment;
    GraficoFragment graficoFragment;
    TerminosFragment terminosFragment;

    TabLayout tabLayout;
    MenuItem prevMenuItem;
    BottomNavigationView navigation;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_terminos:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_calcular:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_grafico:
                    viewPager.setCurrentItem(2);
                    return true;
                case R.id.navigation_help:
                    viewPager.setCurrentItem(3);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       // mTextMessage = (TextView) findViewById(R.id.message);
         navigation = (BottomNavigationView) findViewById(R.id.home_bottom_navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        viewPager = (ViewPager) findViewById(R.id.home_viewpager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                }
                else
                {
                    navigation.getMenu().getItem(0).setChecked(false);
                }
                Log.d("page", "onPageSelected: "+position);
                navigation.getMenu().getItem(position).setChecked(true);
                prevMenuItem = navigation.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        calculadoraFragment=new CalculadoraFragment();
        terminosFragment=new TerminosFragment();
        ayudaFragment=new AyudaFragment();
        graficoFragment=new GraficoFragment();
        adapter.addFragment(terminosFragment);
        adapter.addFragment(calculadoraFragment);
        adapter.addFragment(graficoFragment);
        adapter.addFragment(ayudaFragment);

        viewPager.setAdapter(adapter);


    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
